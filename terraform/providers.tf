# terraform/providers.tf

terraform {
  backend "http" {
    # See .gitlab-ci.yml jobs that use terraform-init -backend-config variables.
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.47"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.47"
    }
    okta = {
      source  = "okta/okta"
      version = "~> 3.35"
    }
  }
  required_version = ">= 1.0.5"
}

# Configure the Okta Provider
provider "okta" {
}
