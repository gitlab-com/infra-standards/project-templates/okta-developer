# terraform/main.tf

# Okta Groups
# -----------------------------------------------------------------------------
# resource "okta_group" "business_technology_department" {
#   name        = "Business Technology 20220901"
#   description = "This is a test group for Business Technology"
# }

resource "okta_group" "department" {
  for_each = var.okta_groups_departments

  name        = "${each.value.name} ${var.dev_suffix}"
  description = "This is a test group for ${each.value.name}"
}

# Okta Group Dynamic Rule for Department Attribute
# -----------------------------------------------------------------------------
# resource "okta_group_rule" "business_technology_department" {
#   name              = "Business Technology Attribute 20220901"
#   expression_value  = "user.department==\"Business Technology\""
#   group_assignments = [
#     00g6ce98dkB8qSDfg5d7
#   ]
# }

resource "okta_group_rule" "department" {
  for_each = var.okta_groups_departments

  name              = "${each.value.name} Attribute ${var.dev_suffix}"
  status            = "ACTIVE"
  expression_type   = "urn:okta:expression:1.0"
  expression_value  = "user.department==\"${each.value.name}\""
  group_assignments = [
    okta_group.department[each.key].id
  ]

  depends_on = [
    okta_group.department
  ]
}
