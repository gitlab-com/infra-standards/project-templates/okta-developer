# terraform/variables.tf

variable "dev_suffix" {
    type = string
    description = "A string that is suffixed to avoid duplicate names during testing redeployment."
}

variable "okta_groups_departments" {
  type = map(object({
    name = string
  }))
  description = "The array of Okta Groups for organization departments."
}
