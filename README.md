# Okta Developer Environment

This is a GitLab project (template) that is used for providing the Terraform scaffolding for an Okta Developer account. This open source for education and forking, however is purpose built for GitLab's internal environments.

This scaffolding provides an example array of Okta groups based on departments in GitLab's organization. You can customize this however you see fit.

## How It Works

### Environment Variables

You need to add a GitLab CI variable in your project for each of the following.

| Variable Key | Description | Required | Example Value |
|--------------|-------------|----------|---------------|
| `OKTA_ORG_NAME` | The subdomain for your Okta organization. | **Yes** | `dev-########` |
| `OKTA_BASE_URL` | The top-level domain for Okta service. In most cases, this should use `okta.com`. | **Yes** | `okta.com` |
| `OKTA_API_TOKEN` | The API token that you create in the Okta UI. It is best practice to create a service account user. | **Yes** | `00oXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX` |

### Variables and Data Arrays

All of our departments are defined in Terraform variable definition files. If you're just getting started, you should start with [okta_groups_departments.auto.tfvars.json](okta_groups_departments.auto.tfvars.json).

### Resources

We have Okta groups, group dynamic rules, and other resources defined in `main.tf`. Each resource uses a `for_each` loop over the data arrays.
